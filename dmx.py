import machine
import utime
from array import array

tx_pin = 17
#pin 17 for LOLIN32 LITE - PIN17 on Board
#pin 2 for WEMOS MINI - PIN D4 on Board

class universe():
    def __init__(self,port):
        self.port = port
        #has to be UARTport 2 - for LOLIN LITE
        #has to be UARTport 1 - for Wemos

        # To check if port is valid
        dmx_uart = machine.UART(self.port)
        del(dmx_uart)

        # First byte is always 0, 512 after that is the 512 channels
        self.dmx_message = array('B', [0] * 513)

    def set_channels(self, message):
        """
        a dict and writes them to the array
        format {channel:value}
        """

        for ch in message:
            self.dmx_message[ch] = message[ch]
        
        print(self.dmx_message)

        # for i, ch in enumerate(channels):
        #     self.dmx_message[ch] = values[i]

    def write_frame(self,timer):
        """
        Send a DMX frame
        """
        # DMX needs a 88us low to begin a frame,
        # 77uS us used because of time it takes to init pin
        dmx_uart = machine.Pin(tx_pin, machine.Pin.OUT)
        dmx_uart.value(0)
        utime.sleep_us(70)
        dmx_uart.value(1)

        # Now turn into a UART port and send DMX data
        dmx_uart = machine.UART(self.port,250000)
        dmx_uart.init(250000, bits=8, parity=None, stop=2)
        #send bytes
        dmx_uart.write(self.dmx_message)
        #Delete as its going to change anyway
        del(dmx_uart)

    def run(self):
        tim = machine.Timer(-1)
        tim.init(period=500, callback=self.write_frame)
        #timer could be shorter if you handle no MQTT but for some dimmers it could cause problems if it is less then 30ms



