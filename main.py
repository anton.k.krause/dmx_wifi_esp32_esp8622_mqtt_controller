import network
import time
import wifimgr
from dmx import universe
from mqttsub import MQTTSUB


wlan = wifimgr.get_connection()
if wlan is None:
    print("Could not initialize the network connection.")
    while True:
        pass  # you shall not pass :D

print("ESP OK")



class DMX:

    def __init__(self):
        self.dmx1 = universe(2) # should be 1 for WEmos and 2 for Lolin Lite  - it is the number of your UART port
        self.dmx1.run()
     
    def read_channel(self):
        with open('wifi.dat') as f:
            lines = f.readlines()
        mqtt = []
        for line in lines:
            ssid, password,broker_ip,client_id,channel = line.strip("\n").split(";")
        channel=int(channel)
        print(channel)
        return channel
    
    def setCH_Value(self,ch,value):
            #for i in range(0,255):
        self.dmx1.set_channels({ch:value})
                
    def MQTT_Callback(self,topic,msg):
        topicStr=str(topic)
        channel=topicStr.split("CH_")[1].strip("'")
        print(channel)
        
        if not channel == "ALL":
            channel=int(channel)
        
            try:    
                value = int(msg)
                print(value)

            except:
                print('not integer')
                value = 0
            
            self.dmx1.set_channels({channel:value})
        
        else:
          
            try:    
                value = int(msg)
                print(value)

            except:
                print('not integer')
                value = 0
              
            for i in range(0,self.read_channel()):
              j=i+1
              j=int(j)
              self.dmx1.set_channels({j:value})

#station=network.WLAN(network.STA_IF)
#station.active(True)
#station.connect(SSID,PW)
#print("Connecting")
#while not station.isconnected():
#    time.sleep(1)
#    print(".")
#print(station.ifconfig())

d=DMX()
m=MQTTSUB(d.MQTT_Callback)
m.start()

