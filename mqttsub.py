import time
from umqtt2 import MQTTClient


class MQTTSUB:
    def __init__(self,callb):
        self.SERVER = self.read_mqtt()[0]
        self.CLIENT_ID=self.read_mqtt()[1]
        self.CH=int(self.read_mqtt()[2])
        self.TOPIC_List = []
        
        for i in range(0,self.CH):
            numberAsString = str(i+1)
            chString=b'DMX_CH_' + numberAsString
            self.TOPIC_List.append(chString)
        
        self.client=MQTTClient(self.CLIENT_ID,self.SERVER)
        self.client.connect()
        print('mqtt_connected')
        self.client.set_callback(callb)
        
        for topic in self.TOPIC_List:
            self.client.subscribe(topic,qos=0)
            print(topic)

        self.client.subscribe(b'DMX_CH_ALL',qos=0)

    def read_mqtt(self):
        with open('wifi.dat') as f:
            lines = f.readlines()
        mqtt = []
        for line in lines:
            ssid, password,broker_ip,client_id,channel = line.strip("\n").split(";")
            mqtt.append(broker_ip)
            mqtt.append(client_id)
            mqtt.append(channel)
        return mqtt
        
    
    def start(self):
        while True:
            self.client.wait_msg()
            time.sleep(1)
